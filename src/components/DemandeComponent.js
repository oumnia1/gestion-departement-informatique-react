import React, {Component} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Dialog} from 'primereact/dialog';
import '../css/DataTable.css';
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import DemandeService from "../services/DemandeService";
import {Image} from "primereact/image";

export class DemandeComponent extends Component {
    //declare emptyDemande
    emptyDemande = {
        type: '',
        demandeur: '',
        objet: '',
        approbateur: null,
        dateDemande: '',
        dateApprobation: '',
        statut: null,
        admin: false
    }
//declare emptyUser
    emptyUser = {
        nom: '',
        prenom: ''
    }
//declare constructor of component
    constructor(props) {
        super(props);
//initialiase the state
        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR"),
            demandes: null,
            demande: this.emptyDemande,
            selectedDemandes: null,
            detailDemandeDialog: false,
            approuverDemandeDialog: false,
            rejeterDemandeDialog: false,
            user: this.emptyUser
        };

        this.selectedUserId = '';
//binding fonctions
        this.typeBodyTemplate = this.typeBodyTemplate.bind(this);
        this.demandeurBodyTemplate = this.demandeurBodyTemplate.bind(this);
        this.statutBodyTemplate = this.statutBodyTemplate.bind(this);
        this.dateDemandeBodyTemplate = this.dateDemandeBodyTemplate.bind(this);
        this.nomObjetBodyTemplate = this.nomObjetBodyTemplate.bind(this);
        this.hideApprouverDemandeDialog = this.hideApprouverDemandeDialog.bind(this);
        this.hideRejeterDemandeDialog = this.hideRejeterDemandeDialog.bind(this);
        this.hideDetailDemandeDialog = this.hideDetailDemandeDialog.bind(this);
        this.approuverDemande = this.approuverDemande.bind(this);
        this.rejeterDemande = this.rejeterDemande.bind(this);
        this.confirmRejeterDemande = this.confirmRejeterDemande.bind(this);
        this.confirmApprouverDemande = this.confirmApprouverDemande.bind(this);
        this.detailDemande = this.detailDemande.bind(this);
        this.getUser = this.getUser.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);
    }
//compoenetDidMount is called after the component is rendered for the first time. It is used to perform any setup or data fetching that needs to happen after the component is rendered.
    componentDidMount() {
        const {currentUser, isAdmin} = this.state;
       //the state object to get the current user and the admin status.
        console.log('CURRENT USER : ' + JSON.stringify(currentUser))
        console.log('IS ADMIN : ' + isAdmin)
//calls the DemandeService.findAll() method which is used to fetch all the demandes, and it uses the response to set the demandes state.
        DemandeService.findAll().then(
            response => {
                console.log(response.data)
                this.setState({demandes: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }
    // functions hideApprouverDemandeDialog,hideRejeterDemandeDialog,hideDetailDemandeDialog setting the state of the corresponding dialog box to false, which will cause the dialog box to be hidden from the user's view
    hideApprouverDemandeDialog() {
        this.setState({approuverDemandeDialog: false});
    }

    hideRejeterDemandeDialog() {
        this.setState({rejeterDemandeDialog: false});
    }

    hideDetailDemandeDialog() {
        this.setState({detailDemandeDialog: false});
    }
//functions to get user by id using findby function
    getUser(id) {
        UserService.findById(id).then(function (response) {
            console.log(response.data)
            //this.setState({user: response.data});
        });
    }
//confirme the demande and set  approuverDemandeDialog=true
    confirmApprouverDemande(demande) {
        this.setState({
            demande,
            approuverDemandeDialog: true
        });
        console.log(demande)
    }
   //rejeter the demande and set  rejeterDemandeDialog=true
    confirmRejeterDemande(demande) {
        this.setState({
            demande,
            rejeterDemandeDialog: true
        });
        console.log(demande)
    }
//approuve the demande 
    approuverDemande() {
        const {demande,currentUser} = this.state;
        let demandeRequest = {
            "idDemande":demande.idDemande,
            "idApprobateur" : currentUser.id,
            "statutCible": 'APPROUVEE'
        };
        DemandeService.traiterDemande(demandeRequest,this.toast)
        //demande.statut = 'APPROUVEE'
        this.setState({
            demande: {...demande},
            approuverDemandeDialog: false
        });
    }
//rejet the demande
    rejeterDemande() {
        const {demande,currentUser} = this.state;
        let demandeRequest = {
            "idDemande":demande.idDemande,
            "idApprobateur" : currentUser.id,
            "statutCible": 'REJETEE'
        };
        DemandeService.traiterDemande(demandeRequest,this.toast)
        demande.statut = 'REJETEE'
        this.setState({
            demande: {...demande},
            rejeterDemandeDialog: false
        });
    }
//open a dialog box displaying the details of the "demande" passed in.
    detailDemande(demande) {
        this.setState({
            demande,
            detailDemandeDialog: true
        });
    }
//this function used to find the index of an element in the "materiels" array that has a specific idMateriel property.
    findIndexById(id) {
        let index = -1;
        for (let i = 0; i < this.state.materiels.length; i++) {
            if (this.state.materiels[i].idMateriel === id) {
                index = i;
                break;
            }
        }

        return index;
    }
//creating a table with the data from the API.
// creating a column for each property of the data.
    typeBodyTemplate(rowData) {
        return rowData.type;
    }

    demandeurBodyTemplate(rowData) {
        return rowData.nomDemandeur;
    }
//render an image based on the statut of the rowData, the possible statut are ENCOURS, APPROUVEE, REJETEE, ANNULEE.
    statutBodyTemplate(rowData) {
        if(rowData.statut == 'ENCOURS'){
            return (
                <Image src={require("../images/inprogress.jpeg")}
                       alt="Image" width="60" height="30"/>
            );
        }
        if(rowData.statut == 'APPROUVEE'){
            return (
                <Image src={require("../images/approved.webp")}
                       alt="Image" width="60" height="30" />
            );
        }
        if(rowData.statut == 'REJETEE'){
            return (
                <Image src={require("../images/rejected.webp")}
                       alt="Image" width="60" height="30"/>
            );
        }
        if(rowData.statut == 'ANNULEE'){
            return (
                <Image src={require("../images/cancelled.jpeg")}
                       alt="Image" width="80" height="40"/>
            );
        }
    }
// render the dateDemande of the rowData.
    dateDemandeBodyTemplate(rowData) {
        return rowData.dateDemande;
    }
// render the type of either materiel or fourniture based on the availability of the data.
    nomObjetBodyTemplate(rowData) {
        if (rowData.materiel) {
            return rowData.materiel.type;
        } else {
            return rowData.fourniture.type;
        }
    }
// render buttons based on the statut of the rowData, the possible actions are Détail, Approuver, Rejeter. It also checks for the state of isAdmin to decide if the buttons are visible or not.
    actionBodyTemplate(rowData) {
        const {isAdmin} = this.state;
        let canProceed = rowData.statut == 'ENCOURS'
        return (
            <React.Fragment>
                <Button icon="pi pi-eye" className="p-button-rounded p-button-outlined p-button-warning mr-2"
                        onClick={() => this.detailDemande(rowData)}
                        tooltip="Détail"
                        tooltipOptions={{position: 'left'}}/>
                {canProceed && (
                    <Button icon="pi pi-thumbs-up"
                            className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.confirmApprouverDemande(rowData)}
                            tooltip="Approuver"
                            tooltipOptions={{position: 'top'}}/>
                )}
                {canProceed && (
                    <Button icon="pi pi-thumbs-down"
                            className="p-button-rounded p-button-outlined p-button-danger mr-2"
                            onClick={() => this.confirmRejeterDemande(rowData)}
                            tooltip="Rejeter"
                            tooltipOptions={{position: 'right'}}/>
                )}
            </React.Fragment>
        );
    }

    render() {
        const {currentUser, isAdmin} = this.state;
        const header = (
            <div className="table-header">
                <h5 className="mx-0 my-1">Liste des demandes </h5>
            </div>
        );

        const approuverDemadeDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideApprouverDemandeDialog}/>
                <Button label="Approuver" icon="pi pi-check" className="p-button-text" onClick={this.approuverDemande}/>
            </React.Fragment>
        );

        const rejeterDemadeDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideRejeterDemandeDialog}/>
                <Button label="Rejeter" icon="pi pi-check" className="p-button-text" onClick={this.rejeterDemande}/>
            </React.Fragment>
        );

        const detailDemadeDialogFooter = (
            <React.Fragment>
                <Button label="OK" icon="pi pi-times" className="p-button-rounded p-button-secondary"
                        onClick={this.hideDetailDemandeDialog}/>
            </React.Fragment>
        );

        const askMaterielDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideAskMaterielDialog}/>
                <Button label="Demander" icon="pi pi-check" className="p-button-text" onClick={this.askMateriel}/>
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el}/>

                <div className="card">

                    <DataTable ref={(el) => this.dt = el}
                               value={this.state.demandes}
                               dataKey="idMateriel" paginator rows={4} rowsPerPageOptions={[4, 10, 25]}
                               paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                               currentPageReportTemplate="Afficher  {first} à {last} de {totalRecords} demandes"
                               header={header} responsiveLayout="scroll">
                        <Column field="demandeur" header="DEMANDEUR" style={{minWidth: '8rem'}}
                                body={this.demandeurBodyTemplate}></Column>
                        <Column field="statut" header="STATUT" sortable style={{minWidth: '8rem'}}
                                body={this.statutBodyTemplate}></Column>
                        <Column field="type" header="TYPE" style={{minWidth: '8rem'}}
                                body={this.typeBodyTemplate}></Column>
                        <Column field="objet" header="OBJET" style={{minWidth: '8rem'}}
                                body={this.nomObjetBodyTemplate}></Column>
                        <Column field="dateDemande" header="DATE DEMANDE" style={{minWidth: '8rem'}}
                                body={this.dateDemandeBodyTemplate}></Column>
                        <Column header="ACTIONS" body={this.actionBodyTemplate} exportable={false}
                                style={{minWidth: '8rem'}}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.approuverDemandeDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={approuverDemadeDialogFooter} onHide={this.hideApprouverDemandeDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.demande &&
                            <span>Voulez-vous approuvez la demande de <b>{this.state.demande.nomDemandeur}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.rejeterDemandeDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={rejeterDemadeDialogFooter} onHide={this.hideRejeterDemandeDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.demande &&
                            <span>Voulez-vous rejeter la demande de <b>{this.state.demande.nomDemandeur}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.detailDemandeDialog} style={{width: '450px'}} header="Détail de la demande"
                        modal footer={detailDemadeDialogFooter} onHide={this.hideDetailDemandeDialog}>
                    {this.state.demande.statut == 'ENCOURS' &&
                        <span>La demande est toujours encours, son statut sera mis à jour dès sont traitement par un adminstrateur </span>}
                    {this.state.demande.statut == 'APPROUVEE' &&
                        <span>La demande à été approuvée par <b>{this.state.demande.nomApprobateur} </b> le <b>{this.state.demande.dateTraitement}</b> </span>}
                    {this.state.demande.statut == 'REJETEE' && this.state.demande.approbateur &&
                        <div>
                            <div>
                                <span>
                                    La demande à été rejetée par <b>{this.state.demande.nomApprobateur} </b> le <b>{this.state.demande.dateTraitement}</b>
                                </span>
                            </div><br/>
                            <div>
                                <span>
                                    Pour savoir plus sur les raisons de ce rejet, veuillez contacter la personne qui à effectué le traitement sur l'email ci-dessous
                                </span>
                            </div><br/>
                            <div align="center">
                                <tr>
                                    <td align="center">
                                        <Image src={require("../images/email.jpeg")}
                                               alt="Image" width="60" height="40"/>
                                    </td>
                                    <td align="center">
                                        <b>  {this.state.demande.approbateur.email}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <Image src={require("../images/bureau.jpeg")}
                                               alt="Image" width="60" height="40" />
                                    </td>
                                    <td align="center">
                                        <b>  Bureau N° {this.state.demande.approbateur.numBureau}</b>
                                    </td>
                                </tr>
                            </div>
                        </div>
                            }
                    {this.state.demande.statut == 'ANNULEE' &&
                        <span>La demande à été annulée par son demandeur <b>{this.state.demande.nomDemandeur} </b> le <b>{this.state.demande.dateTraitement}</b> </span>}
                </Dialog>

            </div>
        );
    }
}
