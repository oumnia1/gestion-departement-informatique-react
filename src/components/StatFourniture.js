import React, {Component} from 'react';
import {Chart} from 'primereact/chart';
import FournitureService from "../services/FournitureService";
import EventBus from "../common/EventBus";

export class StatFournitureComponent extends Component {
//declare constructor
    constructor(props) {
        super(props);

        this.state = {
            chartData: null
        };

        this.lightOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#495057'
                    }
                }
            }
        };
    }

    componentDidMount() {
        FournitureService.findAllFourniture().then(
            response => {
                let data = response.data;
                var types = []
                var quantites = []

                // Calcule de quantite total
                var totalQuantite = 0;
                for (var i = 0; i < data.length; i++) {
                    totalQuantite = totalQuantite + data[i].quantite
                }

                for (var i = 0; i < data.length; i++) {
                    types[i] = data[i].type + ' ( Qte = ' + data[i].quantite + ' ) '
                    let pourcentage = (data[i].quantite * 100 / totalQuantite).toFixed(2)
                    console.log(pourcentage)
                    quantites[i] = pourcentage
                }
                let dataChart = {
                    labels: types,
                    datasets: [
                        {
                            data: quantites
                        }
                    ]
                };
                this.setState({chartData: dataChart});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }

    render() {
        return (
            <div className="card flex justify-content-center ">
                <Chart type="doughnut"
                       data={this.state.chartData}
                       options={this.lightOptions}
                       style={{position: 'relative', width: '45%'}}/>
            </div>
        )
    }
}
