import React, {Component} from 'react';
import {classNames} from 'primereact/utils';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Rating} from 'primereact/rating';
import {Toolbar} from 'primereact/toolbar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import '../css/DataTable.css';
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import {Dropdown} from "primereact/dropdown";
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable'
import {Image} from 'primereact/image';
import MaterielService from "../services/MaterielService";
import {Calendar} from "primereact/calendar";
import {addLocale} from "primereact/api";
import DemandeService from "../services/DemandeService";

export class MaterielComponent extends Component {
//declare objects
    emptyMateriel = {
        type: '',
        numeroInventaire: '',
        dateAcquisition: '',
        utilisateur: null,
        dateAffectation: '',
        nomAffectation: '',
        magasin: null,
        admin: false
    }

    emptyUser = {
        nom: '',
        prenom: ''
    }

//declare constructor
    constructor(props) {
        super(props);

        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR"),
            submitted: false,
            globalFilter: null,
            materiels: null,
            materiel: this.emptyMateriel,
            selectedMateriels: null,
            deleteMaterielDialog: false,
            deleteMaterielsDialog: false,
            materielDialog: false,
            askMaterielDialog: false,
            user: this.emptyUser
        };

        this.allUsers = [];
        UserService.findAllUser().then(response => {
            this.allUsers = response.data.map(user => ({name: user.nom + ' ' + user.prenom, code: user.idUtilisateur}));
            //this.allUsers = response.data.map(user => ({ code: user.nom + ' ' + user.prenom, name: user.nom + ' ' + user.prenom }));
        });

        this.selectedUserId = '';

        this.cols = [
            {field: 'type', header: 'TYPE'},
            {field: 'numeroInventaire', header: 'N° Inventaire'},
            {field: 'dateAcquisition', header: 'DATE ACQUISITION'},
            {field: 'nomAffectation', header: 'AFFECTATION'},
            {field: 'dateAffectation', header: 'DATE AFFECTATION'}
        ];
//define an array of objects called this.exportColumns. Each object in the array represents a column in a table, and has two properties: title and dataKey.
        this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));

        this.leftToolbarTemplate = this.leftToolbarTemplate.bind(this);
        this.rightToolbarTemplate = this.rightToolbarTemplate.bind(this);

        this.typeBodyTemplate = this.typeBodyTemplate.bind(this);
        this.utilisateurBodyTemplate = this.utilisateurBodyTemplate.bind(this);
        this.dateAcquisitionBodyTemplate = this.dateAcquisitionBodyTemplate.bind(this);
        this.dateAffectationBodyTemplate = this.dateAffectationBodyTemplate.bind(this);
        this.numeroInventaireBodyTemplate = this.numeroInventaireBodyTemplate.bind(this);
        this.deleteMateriel = this.deleteMateriel.bind(this);
        this.hideDeleteMaterielDialog = this.hideDeleteMaterielDialog.bind(this);
        this.hideDeleteMaterielsDialog = this.hideDeleteMaterielsDialog.bind(this);
        this.hideAskMaterielDialog = this.hideAskMaterielDialog.bind(this);
        this.confirmDeleteMateriel = this.confirmDeleteMateriel.bind(this);
        this.confirmAskMateriel = this.confirmAskMateriel.bind(this);
        this.saveMateriel = this.saveMateriel.bind(this);
        this.deleteMateriel = this.deleteMateriel.bind(this);
        this.searchMateriel = this.searchMateriel.bind(this);
        this.askMateriel = this.askMateriel.bind(this);

        this.getUser = this.getUser.bind(this);

        this.onDropdownUserChange = this.onDropdownUserChange.bind(this);
        this.ratingBodyTemplate = this.ratingBodyTemplate.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);

        this.openNew = this.openNew.bind(this);
        this.hideDialog = this.hideDialog.bind(this);
        this.exportCSV = this.exportCSV.bind(this);
        this.exportPDF = this.exportPDF.bind(this);
        this.confirmDeleteSelected = this.confirmDeleteSelected.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.format = this.format.bind(this);

        addLocale('fr', {
            firstDayOfWeek: 1,
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Aujourd\'hui',
            clear: 'Vider'
        });
    }

//This function is a lifecycle method in React that is called after the component has been rendered on the page. It sets the state of the component with the current user and isAdmin values and makes a call to MaterielService.findAllMateriel() to fetch all the materiels. If there is an error during the fetch, it sets the state with the error message and logs the user out if the error status is 401.
    componentDidMount() {
        const {currentUser, isAdmin} = this.state;

        console.log('CURRENT USER : ' + JSON.stringify(currentUser))
        console.log('IS ADMIN : ' + isAdmin)

        MaterielService.findAllMateriel(isAdmin).then(
            response => {
                console.log(response.data)
                this.setState({materiels: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }

//This function sets the state of the component to open the materiel dialog box with the emptyMateriel object, sets submitted to false, and sets materielDialog to true.
    openNew() {
        this.setState({
            materiel: this.emptyMateriel,
            submitted: false,
            materielDialog: true
        });
    }

//This function sets the state of the component to close the materiel dialog box by setting submitted to false and materielDialog to false.
    hideDialog() {
        this.setState({
            submitted: false,
            materielDialog: false
        });
    }

//This function sets the state of the component to close the delete materiel dialog box.
    hideDeleteMaterielDialog() {
        this.setState({deleteMaterielDialog: false});
    }

//This function sets the state of the component to close the delete multiple materiels dialog box.
    hideDeleteMaterielsDialog() {
        this.setState({deleteMaterielsDialog: false});
    }

// This function sets the state of the component to close the ask dialog box.
    hideAskMaterielDialog() {
        this.setState({askMaterielDialog: false});
    }

// This function takes an input date and a boolean value as parameters. It formats the date in the format of 'dd-mm-yyyy' if toFront is true and 'yyyy-mm-dd hh:mm:ss' if toFront is false.
    format(inputDate, toFront) {
        let day, month, year, hour, minute, second;
        if (inputDate == null || inputDate == "Invalid Date") {
            return null;
        }
        day = inputDate.getDate();
        month = inputDate.getMonth() + 1;
        year = inputDate.getFullYear();

        day = day.toString().padStart(2, '0');
        month = month.toString().padStart(2, '0');

        if (toFront) {
            return `${day}-${month}-${year}`;
        } else {
            hour = inputDate.getHours();
            minute = inputDate.getMinutes();
            second = inputDate.getSeconds()

            hour = hour.toString().padStart(2, '0');
            minute = minute.toString().padStart(2, '0');
            second = second.toString().padStart(2, '0');

            return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
        }
    }

    //This function takes an id as a parameter and makes a call to UserService.findById() to fetch the user with the given id.
    getUser(id) {
        UserService.findById(id).then(function (response) {
            console.log(response.data)
            //this.setState({user: response.data});
        });
    }

// This function is called when the form is submitted to save the materiel. It sets the submitted state to true and checks if the form is valid. If the form is valid, it creates a new materiel object with the form data, sets the materiel's affectation name and id, formats the date, and calls MaterielService.updateMateriel() if the materiel already exists or MaterielService.addMateriel() if it's a new materiel. It then updates the materiels state and closes the dialog box.
    saveMateriel() {
        let state = {submitted: true};

        if (this.state.materiel.numeroInventaire
            && this.state.materiel.type
            && this.state.materiel.dateAcquisition) {
            let materiels = [...this.state.materiels];
            let materiel = {...this.state.materiel};

            this.allUsers.forEach(element => {
                if (element.code == this.selectedUserId) {
                    materiel['nomAffectation'] = element.name;
                }
            });
            materiel['idAffectation'] = this.selectedUserId;
            const parsed = parseInt(materiel['idAffectation'], 10);
            let dateAffectation = new Date(materiel.dateAffectation)
            let dateAcquisition = new Date(materiel.dateAcquisition)
            materiel.dateAcquisition = this.format(dateAcquisition, false)
            if (isNaN(parsed)) {
                materiel.dateAffectation = null
            } else {
                materiel.dateAffectation = this.format(dateAffectation, false)
            }

            if (this.state.materiel.idMateriel) {
                const index = this.findIndexById(this.state.materiel.idMateriel);
                materiels[index] = materiel;
                console.log('materiel : ' + JSON.stringify(materiel))
                MaterielService.updateMateriel(materiel, this.toast);
                materiel.dateAcquisition = this.format(dateAcquisition, true)
                if (isNaN(parsed)) {
                    materiel.dateAffectation = null
                } else {
                    materiel.dateAffectation = this.format(dateAffectation, true)
                }

            } else {
                MaterielService.addMateriel(materiel);

                materiel.dateAcquisition = this.format(dateAcquisition, true)
                materiel.dateAffectation = this.format(dateAffectation, true)
                materiels.push(materiel);
                this.toast.show({severity: 'success', summary: 'Successful', detail: 'Matériel ajouté', life: 3000});
            }

            state = {
                ...state,
                materiels,
                materielDialog: false,
                materiel
            };
        }

        this.setState(state);
    }

//The editMateriel function is called when the user clicks the "Edit" button for a material in the data table. It sets the component's materiel state to the selected material and sets the materielDialog flag in the component's state to true, which causes the "Edit Material" dialog to be displayed. It also logs the selected material to the console.
    editMateriel(materiel) {
        this.setState({
            materiel: {...materiel},
            materielDialog: true
        });
        console.log(materiel)
    }

//The confirmDeleteMateriel function is called when the user clicks the "Delete" button for a material in the data table. It sets the component's materiel state to the selected material and sets the deleteMaterielDialog flag in the component's state to true, which causes the "Delete Material" confirmation dialog to be displayed.
    confirmDeleteMateriel(materiel) {
        this.setState({
            materiel,
            deleteMaterielDialog: true
        });
    }

//The confirmAskMateriel function is called when the user clicks the "Ask" button for a material in the data table. It sets the component's materiel state to the selected material and sets the askMaterielDialog flag in the component's state to true, which causes the "Ask Material" confirmation dialog to be displayed.
    confirmAskMateriel(materiel) {
        this.setState({
            materiel,
            askMaterielDialog: true
        });
    }

    //The deleteMateriel function is called when the user confirms that they want to delete a material by clicking the "Delete" button in the "Delete Material" confirmation dialog. It first filters the component's materiels state to remove the selected material, then sets the deleteMaterielDialog flag in the component's state to false and the materiel state to an empty object. It also calls the deleteMateriel function from the MaterielService class, which likely makes an API call to delete the material from the server. The function also calls the toast.show function to display a success message to the user.
    deleteMateriel() {
        let materiels = this.state.materiels.filter(val => val.idMateriel !== this.state.materiel.idMateriel);
        this.setState({
            materiels,
            deleteMaterielDialog: false,
            materiel: this.emptyMateriel
        });
        console.log(this.state.materiel.idMateriel)
        MaterielService.deleteMateriel(this.state.materiel.idMateriel);
        this.toast.show({severity: 'success', summary: 'operation réussie', detail: 'Matériel supprimé', life: 3000});
    }

//The askMateriel function is called when the user confirms that they want to ask a material by clicking the "Ask" button in the "Ask Material" confirmation dialog. It creates a new object called 'demande' which contains the id of the current user, the type of the request and the id of the selected material. Then it calls the askMateriel function from the DemandeService class, which likely makes an API call to ask the material from the server. The function also sets the askMaterielDialog flag in the component's state to false.
    askMateriel() {
        const {currentUser} = this.state;
        let materiel = {...this.state.materiel};
        let demande = {
            "idDemandeur": currentUser.id,
            "type": 'MATERIEL',
            "materiel": {"idMateriel": materiel.idMateriel}
        };
        console.log(demande)
        DemandeService.askMateriel(demande, this.toast)
        this.setState({
            askMaterielDialog: false
        });
    }

//The findIndexById function is used to find the index of a material in the materiels state by its idMateriel property. It loops through the materiels array and compares the idMateriel property of each material to the passed in id parameter. If it finds a match, it returns the index of the matching material, otherwise it returns -1.
    findIndexById(id) {
        let index = -1;
        for (let i = 0; i < this.state.materiels.length; i++) {
            if (this.state.materiels[i].idMateriel === id) {
                index = i;
                break;
            }
        }

        return index;
    }

//The exportCSV function is called when the user clicks the "Export CSV" button. It calls the exportCSV function of the dt object, which likely exports the data table's data to a CSV file.
    exportCSV() {
        this.dt.exportCSV();
    }

//The exportPDF function is called when the user clicks the "Export PDF" button. It creates a new instance of the jsPDF library and calls the
    exportPDF() {
        const doc = new jsPDF("p", "px", "a3");
        doc.text("Titre : La liste des enseignants et les matériels qui leurs sont affectés", 20, 10)
        var pdfjs = document.querySelector('#headerPDF');

        doc.autoTable(this.exportColumns, this.state.materiels);
        doc.save('materiel-dep-info.pdf');
    }

//The confirmDeleteSelected function sets the deleteMaterielsDialog state to true, which displays a dialog box asking the user to confirm the deletion of selected materials.
    confirmDeleteSelected() {
        this.setState({deleteMaterielsDialog: true});
    }

//The deleteSelectedMateriels function filters out the selected materials from the array of materials and updates the state with the remaining materials. It also sets the deleteMaterielsDialog state to false and the selectedMateriels state to null. It also shows a toast message to indicate that the operation was successful.
    deleteSelectedMateriels() {
        let users = this.state.materiels.filter(val => !this.state.selectedMateriels.includes(val));
        this.setState({
            users,
            deleteMaterielsDialog: false,
            selectedMateriels: null
        });
        this.toast.show({severity: 'success', summary: 'Opération réussie', detail: 'Materiels supprimés', life: 3000});
    }

//The onInputChange function updates the state of the material object with the value of the input field that the user has changed. onDropdownUserChange function updates the state of the material object with the value of the selected user.
    onInputChange(e, name) {
        const val = (e.target && e.target.value) || '';
        let materiel = {...this.state.materiel};
        materiel[`${name}`] = val;

        this.setState({materiel});
    }

//The leftToolbarTemplate and rightToolbarTemplate functions return JSX elements that are displayed in the left and right sides of the toolbar respectively. These elements include buttons for creating a new material, exporting data to CSV, and exporting data to PDF.
    onDropdownUserChange(e, name) {
        let materiel = {...this.state.materiel};
        materiel[`${name}`] = e.value;
        this.selectedUserId = e.value;
        this.setState({materiel});
    }

    leftToolbarTemplate() {
        const {isAdmin} = this.state;
        if (isAdmin) {
            return (
                <React.Fragment>
                    <Button label="Nouveau matériel" icon="pi pi-plus"
                            className="p-button-raised p-button-secondary p-button-text mr-2"
                            onClick={this.openNew}/>
                    {/*<Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={this.confirmDeleteSelected}
                        disabled={!this.state.selectedUsers || !this.state.selectedUsers.length} />*/}
                </React.Fragment>
            );
        }
    }

//The typeBodyTemplate, numeroInventaireBodyTemplate, dateAcquisitionBodyTemplate, utilisateurBodyTemplate, dateAffectationBodyTemplate, magasinBodyTemplate, ratingBodyTemplate, actionBodyTemplate are functions that return JSX element that are displayed in the different columns of the table. These functions take a row data as an argument and return the appropriate JSX element for the specific column.
    rightToolbarTemplate() {
        return (
            <React.Fragment>
                <Button icon="pi pi-file-excel"
                        className="mr-2 p-button-raised p-button-success p-button-text inline-block"
                        onClick={this.exportCSV}
                        tooltip="Export CSV"
                        tooltipOptions={{position: 'bottom'}}/>
                <Button icon="pi pi-file-pdf" className="p-button-raised p-button-danger p-button-text inline-block"
                        onClick={this.exportPDF}
                        tooltip="Export PDF"
                        tooltipOptions={{position: 'bottom'}}
                />
            </React.Fragment>
        )
    }

    typeBodyTemplate(rowData) {
        return rowData.type;
    }

    numeroInventaireBodyTemplate(rowData) {
        return rowData.numeroInventaire;
    }

    dateAcquisitionBodyTemplate(rowData) {
        return rowData.dateAcquisition;
    }

    utilisateurBodyTemplate(rowData) {
        if (rowData.nomAffectation) {
            return rowData.nomAffectation;
        } else {
            return (
                <Image src={require("../images/disponible-check.jpeg")}
                       alt="Image" width="80" height="40"/>
            );
        }
    }

    dateAffectationBodyTemplate(rowData) {
        if (rowData.dateAffectation) {
            return rowData.dateAffectation;
        } else {
            return (
                <Image src={require("../images/not-available.webp")}
                       alt="Image" width="80" height="40"/>
            );
        }
    }

    magasinBodyTemplate(rowData) {
        return rowData.magasin.nom;
    }

    ratingBodyTemplate(rowData) {
        return <Rating value={rowData.rating} readOnly cancel={false}/>;
    }

    actionBodyTemplate(rowData) {
        const {isAdmin} = this.state;
        return (
            <React.Fragment>
                {isAdmin ? (
                    <Button icon="pi pi-pencil"
                            className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editMateriel(rowData)}
                            tooltip="Modifier"
                            tooltipOptions={{position: 'left'}}/>
                ) : (
                    <Button icon="pi pi-eye" className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editMateriel(rowData)}
                            tooltip="Visualiser"
                            tooltipOptions={{position: 'top'}}/>
                )}

                {isAdmin && (
                    <Button icon="pi pi-trash"
                            className="p-button-rounded p-button-outlined p-button-danger mr-2"
                            onClick={() => this.confirmDeleteMateriel(rowData)}
                            tooltip="Supprimer"
                            tooltipOptions={{position: 'top'}}/>
                )}
                {!rowData.nomAffectation && (
                    <Button icon="pi pi-cart-plus"
                            className="p-button-rounded p-button-outlined p-button-info"
                            onClick={() => this.confirmAskMateriel(rowData)}
                            tooltip="Demander"
                            tooltipOptions={{position: 'right'}}/>
                )}
            </React.Fragment>
        );
    }

    searchMateriel(event) {
        //this.setState({ globalFilter: event.target.value })
        let type = event.target.value;
        const {isAdmin} = this.state;

        if (type.length == 0) {
            MaterielService.findAllMateriel(isAdmin)
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        materiels: response.data
                    });
                    console.log(this.state.materiels)
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            MaterielService.findMaterielByType(type)
                .then(response => {
                    this.setState({
                        materiels: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    render() {
        const {currentUser, isAdmin} = this.state;
        const header = (
            <div className="table-header">
                <h5 className="mx-0 my-1">Liste du matériel </h5>
                <span className="p-input-icon-left">
                    <i className="pi pi-search"/>
                    <InputText type="search" onInput={(e) => this.searchMateriel(e)} placeholder="Chercher..."/>
                </span>
            </div>
        );

        const materielDialogFooter = (
            <React.Fragment>
                {isAdmin && (
                    <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}
                {isAdmin && (
                    <Button label="Enregistrer" icon="pi pi-check" className="p-button-text" onClick={this.saveMateriel}/>
                )}
                {!isAdmin && (
                    <Button label="Retour" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}
            </React.Fragment>
        );
        const deleteMaterielDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideDeleteMaterielDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text" onClick={this.deleteMateriel}/>
            </React.Fragment>
        );
        const askMaterielDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideAskMaterielDialog}/>
                <Button label="Demander" icon="pi pi-check" className="p-button-text" onClick={this.askMateriel}/>
            </React.Fragment>
        );
        const deleteMaterielsDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideDeleteMaterielsDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text"
                        onClick={this.deleteSelectedMateriels}/>
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el}/>

                <div className="card">
                    <Toolbar className="mb-4" left={this.leftToolbarTemplate}
                             right={this.rightToolbarTemplate}></Toolbar>

                    <DataTable ref={(el) => this.dt = el}
                               value={this.state.materiels}
                               selection={this.state.selectedMateriels}
                               onSelectionChange={(e) => this.setState({selectedMateriels: e.value})}
                               dataKey="idMateriel" paginator rows={4} rowsPerPageOptions={[4, 10, 25]}
                               paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                               currentPageReportTemplate="Afficher  {first} à {last} de {totalRecords} matériels"
                               globalFilter={this.state.globalFilter} header={header} responsiveLayout="scroll">
                        {/*<Column selectionMode={this.state.isAdmin ? "multiple":"single"} headerStyle={{ width: '3rem' }} exportable={false}></Column>*/}
                        <Column field="type" header="Type" sortable style={{minWidth: '8rem'}}
                                body={this.typeBodyTemplate}></Column>
                        <Column field="numeroInventaire" header="N° INVENTAIRE"
                                body={this.numeroInventaireBodyTemplate}></Column>
                        <Column field="dateAcquisition" header="DATE ACQUISITION"
                                body={this.dateAcquisitionBodyTemplate} style={{minWidth: '8rem'}}></Column>
                        {isAdmin && (
                            <Column field="nomAffectation" header="AFFECTATION" body={this.utilisateurBodyTemplate}
                                    style={{minWidth: '5rem'}} align={"center"}></Column>
                        )}
                        {isAdmin && (
                            <Column field="dateAffectation" header="DATE AFFECTATION"
                                    body={this.dateAffectationBodyTemplate} style={{minWidth: '15rem'}}
                                    align={"center"}></Column>
                        )}
                        <Column header="ACTIONS" body={this.actionBodyTemplate} exportable={false}
                                style={{minWidth: '8rem'}}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.materielDialog} header="Informations du matériel" modal
                        className="p-fluid auto" footer={materielDialogFooter} onHide={this.hideDialog}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="type">Type {this.state.submitted && !this.state.materiel.type &&
                                        <small className="p-error">(*) Obligatoire</small>}</label>
                                    <InputText id="type" value={this.state.materiel.type}
                                               onChange={(e) => this.onInputChange(e, 'type')} required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.materiel.type})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                            <td>
                                <div className="field">
                                    <label htmlFor="numeroInventaire">N°
                                        Inventaire {this.state.submitted && !this.state.materiel.numeroInventaire &&
                                            <small className="p-error">(*) Obligatoire</small>}</label>
                                    <InputText id="numeroInventaire" value={this.state.materiel.numeroInventaire}
                                               onChange={(e) => this.onInputChange(e, 'numeroInventaire')} required
                                               autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.materiel.numeroInventaire})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="dateAcquisition">Date
                                        d'acquisition {this.state.submitted && !this.state.materiel.dateAcquisition &&
                                            <small className="p-error">(*) Obligatoire</small>}</label>
                                    {/*<InputText id="dateAcquisition" value={this.state.materiel.dateAcquisition} onChange={(e) => this.onInputChange(e, 'dateAcquisition')}
                                               required autoFocus
                                               className={classNames({ 'p-invalid': this.state.submitted && !this.state.materiel.dateAcquisition })} />*/}
                                    <Calendar value={new Date(this.state.materiel.dateAcquisition)}
                                              onChange={(e) => this.onInputChange(e, 'dateAcquisition')}
                                              showIcon
                                              dateFormat={'dd-mm-yy'}
                                              locale="fr"
                                              required autoFocus
                                              className={classNames({'p-invalid': this.state.submitted && !this.state.materiel.dateAcquisition})}
                                              disabled={!isAdmin}/>
                                </div>
                            </td>
                            {isAdmin && (
                                <td>
                                    <div className="field">
                                        <label htmlFor="utilisateur">Afféctation</label>
                                        <Dropdown value={this.state.materiel.nomAffectation}
                                                  options={this.allUsers}
                                                  optionValue="code"
                                                  optionLabel="name"
                                                  onChange={(e) => this.onDropdownUserChange(e, 'nomAffectation')}
                                                  placeholder="Sélectionnez un utilisateur"
                                                  autoFocus filter showClear filterBy="name"
                                                  className={classNames({'p-invalid': this.state.submitted && !this.state.materiel.nomAffectation})}/>

                                    </div>
                                </td>
                            )}
                        </tr>
                        <tr>
                            {isAdmin && (
                                <td>
                                    <div className="field">
                                        <label htmlFor="dateAffectation">Date d'afféctation</label>
                                        {/*<InputText id="dateAffectation" value={this.state.materiel.dateAffectation}
                                               onChange={(e) => this.onInputChange(e, 'dateAffectation')}
                                               required autoFocus
                                               className={classNames({ 'p-invalid': this.state.submitted && !this.state.materiel.dateAffectation })} />*/}
                                        <Calendar value={new Date(this.state.materiel.dateAffectation)}
                                                  onChange={(e) => this.onInputChange(e, 'dateAffectation')}
                                                  showIcon
                                                  dateFormat={'dd-mm-yy'}
                                                  locale="fr"
                                                  autoFocus
                                                  className={classNames({'p-invalid': this.state.submitted && !this.state.materiel.dateAffectation})}/>
                                    </div>
                                </td>
                            )}
                            <td>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Dialog>

                <Dialog visible={this.state.deleteMaterielDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={deleteMaterielDialogFooter} onHide={this.hideDeleteMaterielDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.materiel &&
                            <span>Confirmez-vous la suppresion de <b>{this.state.materiel.type}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.deleteMaterielsDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={deleteMaterielsDialogFooter} onHide={this.hideDeleteMaterielsDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.materiel && <span>Confirmez-vous la suppresion de la liste séléctionnées?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.askMaterielDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={askMaterielDialogFooter} onHide={this.hideAskMaterielDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.materiel &&
                            <span>Confirmez-vous la demande du materiel <b>{this.state.materiel.type}</b>?</span>}
                    </div>
                </Dialog>
            </div>
        );
    }
}
