import React, {Component} from 'react';
import {classNames} from 'primereact/utils';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import '../css/DataTable.css';
import UserService from "../services/UserService";
import EventBus from "../common/EventBus";
import AuthService from "../services/AuthService";
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable'
import {Image} from 'primereact/image';
import DemandeService from "../services/DemandeService";
import FournitureService from "../services/FournitureService";
import {InputNumber} from "primereact/inputnumber";

export class FournitureComponent extends Component {
//declare 2 objects
    emptyFourniture = {
        type: '',
        quantite: '',
        magasin: null,
        admin: false
    }


    emptyUser = {
        nom: '',
        prenom: ''
    }

//create constructor of the componenet
    constructor(props) {
        super(props);

        this.state = {
            currentUser: AuthService.getCurrentUser(),
            isAdmin: AuthService.getCurrentUser().roles.includes("ADMINISTRATEUR"),
            submitted: false,
            globalFilter: null,
            fournitures: null,
            fourniture: this.emptyFourniture,
            selectedFournitures: null,
            deleteFournitureDialog: false,
            deleteFournituresDialog: false,
            fournitureDialog: false,
            askFournitureDialog: false,
            user: this.emptyUser,
            quantiteDemande: 1
        };

        this.selectedUserId = '';

        this.cols = [
            {field: 'type', header: 'TYPE'},
            {field: 'quantite', header: 'QUANTITE'}
        ];
//bind the functions
        this.exportColumns = this.cols.map(col => ({title: col.header, dataKey: col.field}));

        this.leftToolbarTemplate = this.leftToolbarTemplate.bind(this);
        this.rightToolbarTemplate = this.rightToolbarTemplate.bind(this);

        this.typeBodyTemplate = this.typeBodyTemplate.bind(this);

        this.hideDeleteFournitureDialog = this.hideDeleteFournitureDialog.bind(this);
        this.hideDeleteFournituresDialog = this.hideDeleteFournituresDialog.bind(this);
        this.hideAskFournitureDialog = this.hideAskFournitureDialog.bind(this);
        this.saveFourniture = this.saveFourniture.bind(this);
        this.editFourniture = this.editFourniture.bind(this);
        this.confirmAskFourniture = this.confirmAskFourniture.bind(this);
        this.askFourniture = this.askFourniture.bind(this);
        this.deleteFourniture = this.deleteFourniture.bind(this);
        this.searchFourniture = this.searchFourniture.bind(this);

        this.getUser = this.getUser.bind(this);

        this.onDropdownUserChange = this.onDropdownUserChange.bind(this);
        this.actionBodyTemplate = this.actionBodyTemplate.bind(this);

        this.openNew = this.openNew.bind(this);
        this.hideDialog = this.hideDialog.bind(this);
        this.exportCSV = this.exportCSV.bind(this);
        this.exportPDF = this.exportPDF.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

//The function then makes a call to the "FournitureService.findAllFourniture()" method which likely makes an API call to retrieve all the Fourniture data. On success, the response data is saved to the component's state object as an array called "fournitures". If there is an error, the error message is saved to the state object's "content" property. If the error is a 401 Unauthorized error, it dispatches a "logout" event.
    componentDidMount() {
        const {currentUser, isAdmin} = this.state;

        console.log('CURRENT USER : ' + JSON.stringify(currentUser))
        console.log('IS ADMIN : ' + isAdmin)

        FournitureService.findAllFourniture().then(
            response => {
                console.log(response.data)
                this.setState({fournitures: response.data});
            },
            error => {
                this.setState({
                    content:
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                });

                if (error.response && error.response.status === 401) {
                    EventBus.dispatch("logout");
                }
            });
    }

//openNew() function opens a dialog box to create a new fourniture. It sets the state of the component to show the dialog box, and reset the form values in the state.
    openNew() {
        this.setState({
            fourniture: this.emptyFourniture,
            submitted: false,
            fournitureDialog: true
        });
    }

//hideDialog() function closes the dialog box and resets the form values in the state.
    hideDialog() {
        this.setState({
            submitted: false,
            fournitureDialog: false
        });
    }

//hideDeleteFournitureDialog() function closes the delete confirmation dialog box for a single fourniture.
    hideDeleteFournitureDialog() {
        this.setState({deleteFournitureDialog: false});
    }

//hideDeleteFournituresDialog() function closes the delete confirmation dialog box for multiple fournitures.
    hideDeleteFournituresDialog() {
        this.setState({deleteFournituresDialog: false});
    }

//hideAskFournitureDialog() function closes the dialog box that asks the user to confirm the fourniture request.
    hideAskFournitureDialog() {
        this.setState({askFournitureDialog: false});
    }

//get user by id using findbyid function
    getUser(id) {
        UserService.findById(id).then(function (response) {
            console.log(response.data)
            //this.setState({user: response.data});
        });
    }

//This function is used to save a fourniture (equipment) to the system. It first sets the "submitted" state to true, and then checks if the fourniture has a type and quantity. If it does, it will either update an existing fourniture or add a new one. The fourniture and fournitures state are then updated and the fournitureDialog state is set to false. A success message is displayed using the "toast" component.
    saveFourniture() {
        let state = {submitted: true};

        if (this.state.fourniture.type
            && this.state.fourniture.quantite) {
            let fournitures = [...this.state.fournitures];
            let fourniture = {...this.state.fourniture};
            if (this.state.fourniture.idFourniture) {
                const index = this.findIndexById(this.state.fourniture.idFourniture);
                fournitures[index] = fourniture;
                console.log('fourniture : ' + JSON.stringify(fourniture))
                FournitureService.updateFourniture(fourniture, this.toast)
            } else {
                FournitureService.addFourniture(fourniture, this.toast)
                fournitures.push(fourniture);
                this.toast.show({severity: 'success', summary: 'Successful', detail: 'Fourniture ajouté', life: 3000});
            }

            state = {
                ...state,
                fournitures,
                fournitureDialog: false,
                fourniture
            };
        }
        this.setState(state);
    }

//set the state of the component to include the selected "fourniture" object and opens a dialog to edit it.
    editFourniture(fourniture) {
        this.setState({
            fourniture: {...fourniture},
            fournitureDialog: true
        });
        console.log(fourniture)
    }

//sets the state of the component to include the selected "fourniture" object and opens a dialog to confirm its deletion.
    confirmDeleteFourniture(fourniture) {
        this.setState({
            fourniture,
            deleteFournitureDialog: true
        });
    }

//sets the state of the component to include the selected "fourniture" object and opens a dialog to confirm a request for it.
    confirmAskFourniture(fourniture) {
        this.setState({
            fourniture,
            askFournitureDialog: true
        });
    }

//deletes the "fourniture" object from the state of the component and from the server, and closes the confirmation dialog.
    deleteFourniture() {
        let fournitures = this.state.fournitures.filter(val => val.idFourniture !== this.state.fourniture.idFourniture);
        this.setState({
            fournitures,
            deleteFournitureDialog: false,
            fourniture: this.emptyFourniture
        });
        console.log(this.state.fourniture.idFourniture)
        FournitureService.deleteFourniture(this.state.fourniture.idFourniture, this.toast)
        //this.toast.show({ severity: 'success', summary: 'operation réussie', detail: 'Matériel supprimé', life: 3000 });
    }

//sends a request for the selected "fourniture" object to the server and closes the confirmation dialog.
    askFourniture() {
        const {currentUser, quantiteDemande} = this.state;
        let fourniture = {...this.state.fourniture};
        let demande = {
            "idDemandeur": currentUser.id,
            "type": 'FOURNITURE',
            "fourniture": {"idFourniture": fourniture.idFourniture},
            "quantite": quantiteDemande
        };
        console.log(demande)
        DemandeService.askMateriel(demande, this.toast)
        this.setState({
            askFournitureDialog: false
        });
    }

//finds the index of the "fourniture" object with the specified "id" in the state of the component.
    findIndexById(id) {
        let index = -1;
        console.log(this.state.fournitures)
        for (let i = 0; i < this.state.fournitures.length; i++) {
            if (this.state.fournitures[i].idFourniture === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    //  exports the "fourniture" data as a CSV file
    exportCSV() {
        this.dt.exportCSV();
    }

    //exports the "fourniture" data as a PDF file.
    exportPDF() {
        const doc = new jsPDF("p", "px", "a3");
        doc.text("Titre : Gestion de Departement Informatique", 20, 10)
        var pdfjs = document.querySelector('#headerPDF');

        doc.autoTable(this.exportColumns, this.state.materiels);
        doc.save('fourniture-dep-info.pdf');
    }

//updates the state of the component with the value of the input field when it changes.
    onInputChange(e, name) {
        const val = (e.target && e.target.value) || '';
        let fourniture = {...this.state.fourniture};
        fourniture[`${name}`] = val;

        this.setState({fourniture});
    }

    // updates the state of the component with the selected user when the dropdown value changes.
    onDropdownUserChange(e, name) {
        let fourniture = {...this.state.fourniture};
        fourniture[`${name}`] = e.value;
        this.selectedUserId = e.value;
        this.setState({fourniture});
    }

//Renders a button to add a new supply (only visible to users with "admin" status)
    leftToolbarTemplate() {
        const {isAdmin} = this.state;
        if (isAdmin) {
            return (
                <React.Fragment>
                    <Button label="Nouvelle fourniture" icon="pi pi-plus"
                            className="p-button-raised p-button-secondary p-button-text mr-2"
                            onClick={this.openNew}/>
                    {/*<Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={this.confirmDeleteSelected}
                        disabled={!this.state.selectedUsers || !this.state.selectedUsers.length} />*/}
                </React.Fragment>
            );
        }
    }

//Renders buttons to export the table data as a CSV or PDF file.
    rightToolbarTemplate() {
        return (
            <React.Fragment>
                <Button icon="pi pi-file-excel"
                        className="mr-2 p-button-raised p-button-success p-button-text inline-block"
                        onClick={this.exportCSV}
                        tooltip="Export CSV"
                        tooltipOptions={{position: 'bottom'}}/>
                <Button icon="pi pi-file-pdf" className="p-button-raised p-button-danger p-button-text inline-block"
                        onClick={this.exportPDF}
                        tooltip="Export PDF"
                        tooltipOptions={{position: 'bottom'}}
                />
            </React.Fragment>
        )
    }

    typeBodyTemplate(rowData) {
        return rowData.type;
    }

    quantiteBodyTemplate(rowData) {
        return rowData.quantite;
    }

    magasinBodyTemplate(rowData) {
        return rowData.magasin.nom;
    }

//Renders a set of buttons for interacting with a given row of data. Depending on whether the user is an admin, the buttons will either be for editing or viewing the supply, deleting it, or requesting more of it.
    actionBodyTemplate(rowData) {
        const {isAdmin} = this.state;
        return (
            <React.Fragment>
                {isAdmin ? (
                    <Button icon="pi pi-pencil"
                            className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editFourniture(rowData)}
                            tooltip="Modifier"
                            tooltipOptions={{position: 'left'}}/>
                ) : (
                    <Button icon="pi pi-eye" className="p-button-rounded p-button-outlined p-button-success mr-2"
                            onClick={() => this.editFourniture(rowData)}
                            tooltip="Visualiser"
                            tooltipOptions={{position: 'top'}}/>
                )}

                {isAdmin && (
                    <Button icon="pi pi-trash"
                            className="p-button-rounded p-button-outlined p-button-danger mr-2"
                            onClick={() => this.confirmDeleteFourniture(rowData)}
                            tooltip="Supprimer"
                            tooltipOptions={{position: 'top'}}/>
                )}
                {rowData.quantite > 0 && (
                    <Button icon="pi pi-cart-plus"
                            className="p-button-rounded p-button-outlined p-button-info"
                            onClick={() => this.confirmAskFourniture(rowData)}
                            tooltip="Demander"
                            tooltipOptions={{position: 'right'}}/>
                )}
            </React.Fragment>
        );
    }

//This function takes in an event and search the supplies based on the 'type' of the supply. It filters the table data based on the search input of the user, either by calling the FournitureService.finAllFourniture() method if the input is empty or calling FournitureService.findFournitureByType(type) if there is an input and updates the state with the filtered data.
    searchFourniture(event) {
        let type = event.target.value;
        const {isAdmin} = this.state;

        if (type.length == 0) {
            FournitureService.findAllFourniture()
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        fournitures: response.data
                    });
                    console.log(this.state.fournitures)
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            FournitureService.findFournitureByType(type)
                .then(response => {
                    this.setState({
                        fournitures: response.data
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    render() {
        const {currentUser, isAdmin} = this.state;
        const header = (
            <div className="table-header">
                <h5 className="mx-0 my-1">Liste des fournitures </h5>
                <span className="p-input-icon-left">
                    <i className="pi pi-search"/>
                    <InputText type="search" onInput={(e) => this.searchFourniture(e)} placeholder="Chercher..."/>
                </span>
            </div>
        );

        const fournitureDialogFooter = (
            <React.Fragment>
                {isAdmin && (
                    <Button label="Annuler" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}
                {isAdmin && (
                    <Button label="Enregistrer" icon="pi pi-check" className="p-button-text" onClick={this.saveFourniture}/>
                )}
                {!isAdmin && (
                    <Button label="Retour" icon="pi pi-times" className="p-button-text" onClick={this.hideDialog}/>
                )}

            </React.Fragment>
        );
        const deleteFournitureDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideDeleteFournitureDialog}/>
                <Button label="Confirmer" icon="pi pi-check" className="p-button-text" onClick={this.deleteFourniture}/>
            </React.Fragment>
        );
        const askFournitureDialogFooter = (
            <React.Fragment>
                <Button label="Annuler" icon="pi pi-times" className="p-button-text"
                        onClick={this.hideAskFournitureDialog}/>
                <Button label="Demander" icon="pi pi-check" className="p-button-text" onClick={this.askFourniture}/>
            </React.Fragment>
        );

        return (
            <div className="datatable-crud-demo">
                <Toast ref={(el) => this.toast = el}/>

                <div className="card">
                    <Toolbar className="mb-4" left={this.leftToolbarTemplate}
                             right={this.rightToolbarTemplate}></Toolbar>

                    <DataTable ref={(el) => this.dt = el}
                               value={this.state.fournitures}
                               selection={this.state.selectedFournitures}
                               onSelectionChange={(e) => this.setState({selectedFournitures: e.value})}
                               dataKey="idFourniture" paginator rows={4} rowsPerPageOptions={[4, 10, 25]}
                               paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                               currentPageReportTemplate="Afficher  {first} à {last} de {totalRecords} fournitures"
                               globalFilter={this.state.globalFilter} header={header} responsiveLayout="scroll">
                        {/*<Column selectionMode={this.state.isAdmin ? "multiple":"single"} headerStyle={{ width: '3rem' }} exportable={false}></Column>*/}
                        <Column field="type" header="Type" sortable body={this.typeBodyTemplate}></Column>
                        <Column field="quantite" header="QUANTITE" style={{minWidth: '8rem'}}
                                body={this.quantiteBodyTemplate}></Column>
                        <Column header="ACTIONS" body={this.actionBodyTemplate} exportable={false}
                                style={{minWidth: '8rem'}}></Column>
                    </DataTable>
                </div>

                <Dialog visible={this.state.fournitureDialog} header="Informations fourniture" modal
                        className="p-fluid auto" footer={fournitureDialogFooter} onHide={this.hideDialog}>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div className="field">
                                    <label htmlFor="type">Type {this.state.submitted && !this.state.fourniture.type &&
                                        <small className="p-error">(*) Obligatoire</small>}</label>
                                    <InputText id="type" value={this.state.fourniture.type}
                                               onChange={(e) => this.onInputChange(e, 'type')}
                                               required autoFocus
                                               className={classNames({'p-invalid': this.state.submitted && !this.state.fourniture.type})}
                                               disabled={!isAdmin}/>

                                </div>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <div className="field">
                                    <label htmlFor="quantite">Quantité
                                         {this.state.submitted && !this.state.fourniture.quantite &&
                                            <small className="p-error">(*) Obligatoire</small>}</label>
                                    {/*<InputText id="quantite" value={this.state.fourniture.quantite}
                                               onChange={(e) => this.onInputChange(e, 'quantite')} required
                                               autoFocus
                                               className={classNames({ 'p-invalid': this.state.submitted
                                                       && !this.state.fourniture.numeroInventaire })} />*/}
                                    <InputNumber inputId="horizontal" value={this.state.fourniture.quantite}
                                                 onValueChange={(e) => this.onInputChange(e, 'quantite')}
                                                 showButtons buttonLayout="horizontal" step={1}
                                                 decrementButtonClassName="p-button-secondary"
                                                 incrementButtonClassName="p-button-secondary"
                                                 incrementButtonIcon="pi pi-plus"
                                                 decrementButtonIcon="pi pi-minus" min={0}
                                                 disabled={!isAdmin}/>


                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </Dialog>

                <Dialog visible={this.state.deleteFournitureDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={deleteFournitureDialogFooter} onHide={this.hideDeleteFournitureDialog}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle mr-3" style={{fontSize: '2rem'}}/>
                        {this.state.fourniture &&
                            <span>Confirmez-vous la suppresion de <b>{this.state.fourniture.type}</b>?</span>}
                    </div>
                </Dialog>

                <Dialog visible={this.state.askFournitureDialog} style={{width: '450px'}} header="Confirmation" modal
                        footer={askFournitureDialogFooter} onHide={this.hideAskFournitureDialog}>
                    <div className="confirmation-content">
                        <div align="center">
                            <h5>Quantité</h5>
                            <InputNumber inputId="minmax-buttons"
                                         value={this.state.quantiteDemande}
                                         onValueChange={(e) => this.setState({quantiteDemande: e.value})}
                                         mode="decimal" showButtons min={1} max={this.state.fourniture.quantite}/>
                        </div>
                        <div>
                            {this.state.fourniture &&
                                <span>Confirmez-vous la demande de fourniture <b>{this.state.fourniture.type}</b>?</span>}
                        </div>
                    </div>
                </Dialog>
            </div>
        );
    }
}
