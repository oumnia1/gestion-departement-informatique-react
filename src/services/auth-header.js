export default function authHeader() {
  // used as a header for an API call. If no user object is found in local storage, it returns an empty object.
  const user = JSON.parse(localStorage.getItem('user'));

  if (user && user.accessToken) {
    return { Authorization: 'Bearer ' + user.accessToken }; // for Spring Boot back-end
  } else {
    return {};
  }
}
