import axios from 'axios';
import authHeader from './auth-header';
import { baseUrl } from './AuthService';
const API_URL = `${baseUrl}/materiel`;
/*1. It's defining the API_URL as a constant.
2. It's defining a findAllMateriel() method that returns a promise.
3. It's defining a findMaterielByType() method that returns a promise.
4. It's defining an addMateriel() method*/


class MaterielService {

    findAllMateriel(isAdmin){
        if(isAdmin){
            return axios.get(API_URL + '/all', { headers: authHeader() });
        } else {
            return axios.get(API_URL + '/disponible', { headers: authHeader() });
        }
    }

    findMaterielByType(type){
        return axios.get(API_URL + '/type/'+type, { headers: authHeader() });
    }

    addMateriel(materiel){
        return axios.post(API_URL + '/add',materiel ,{ headers: authHeader() })
            .then(response => {
                console.log('addUser result : ' + JSON.stringify(response));
            });
    }

    updateMateriel(materiel,toast){
        axios.put(API_URL + '/update',materiel ,{ headers: authHeader() })
            .then(response => {
                console.log(response.status);
                toast.show({ severity: 'success', summary: 'Successful', detail: 'Matériel modifié', life: 3000 });
            }).catch(function (error){
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
            toast.show({severity:'error', summary: 'Erreur', detail:'Message Content', life: 3000});

        });
    }

    deleteMateriel(id){
        return axios.delete(API_URL + '/delete/' + id,{ headers: authHeader() })
            .then(response => {
                console.log(response);
            });
    }

}

export default new MaterielService();
